package eim.yar.immo_task;

import android.app.Application;

import eim.yar.immo_task.presentation.injection.component.ApplicationComponent;
import eim.yar.immo_task.presentation.injection.component.DaggerApplicationComponent;
import eim.yar.immo_task.presentation.injection.module.ApplicationModule;

/**
 * Android main application.
 */
public class IMMOTaskApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    /**
     * Initialize dagger application component.
     */
    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

}
