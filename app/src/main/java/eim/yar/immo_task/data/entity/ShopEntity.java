package eim.yar.immo_task.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Shop entity model class for data layer.
 */
@Entity(tableName = "shops")
@SuppressWarnings("PMD.DataClass")
public class ShopEntity {

    @PrimaryKey
    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("address")
    private String address;

    @ColumnInfo(name = "metro_string")
    @SerializedName("metro_string")
    private String metroString;

    @ColumnInfo(name = "opening_hours")
    @SerializedName("opening_hours")
    private String openingHours;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    /**
     * Field to store value of distance from current position to the shop.
     * The field is required to get list of shops from the database sorted by distance.
     *
     * SQLite has lack of possibilities to calculate distance between its objects,
     * so distance would be calculated outside of the database and then stored in it.
     */
    private float distance;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMetroString() {
        return metroString;
    }

    public void setMetroString(String metroString) {
        this.metroString = metroString;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }
}
