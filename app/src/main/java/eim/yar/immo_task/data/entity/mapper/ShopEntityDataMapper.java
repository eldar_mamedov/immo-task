package eim.yar.immo_task.data.entity.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import eim.yar.immo_task.data.entity.ShopEntity;
import eim.yar.immo_task.domain.Shop;

/**
 * Mapper class used to transform {@link ShopEntity} (in the data layer) to {@link Shop} in the
 * domain layer.
 */
@Singleton
public class ShopEntityDataMapper {

    /**
     * Constructs a {@link ShopEntityDataMapper}.
     */
    @Inject
    public ShopEntityDataMapper() {
        // empty
    }

    /**
     * Transform a {@link ShopEntity} into an {@link Shop}.
     *
     * @param shopEntity Object to be transformed.
     * @return {@link Shop} if valid {@link ShopEntity} otherwise null.
     */
    public Shop transform(ShopEntity shopEntity) {
        Shop shop = null;
        if (shopEntity != null) {
            shop = new Shop(shopEntity.getId());
            shop.setName(shopEntity.getName());
            shop.setAddress(shopEntity.getAddress());
            shop.setMetro(shopEntity.getMetroString());
            shop.setOpeningHours(shopEntity.getOpeningHours());
            shop.setDistance(shopEntity.getDistance());
        }
        return shop;
    }

    /**
     * Transform a List of {@link ShopEntity} into a Collection of {@link Shop}.
     *
     * @param shopEntityCollection Object Collection to be transformed.
     * @return {@link Shop} if valid {@link ShopEntity} otherwise null.
     */
    public List<Shop> transform(Collection<ShopEntity> shopEntityCollection) {
        final List<Shop> shopList = new ArrayList<>();
        for (ShopEntity shopEntity : shopEntityCollection) {
            final Shop shop = transform(shopEntity);
            if (shop != null) {
                shopList.add(shop);
            }
        }
        return shopList;
    }
}
