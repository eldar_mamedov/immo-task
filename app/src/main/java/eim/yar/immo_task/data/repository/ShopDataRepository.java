package eim.yar.immo_task.data.repository;

import android.location.Location;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import eim.yar.immo_task.data.entity.ShopEntity;
import eim.yar.immo_task.data.entity.mapper.ShopEntityDataMapper;
import eim.yar.immo_task.data.repository.dataloader.ShopDataLoader;
import eim.yar.immo_task.data.repository.datastore.ShopDataStore;
import eim.yar.immo_task.data.repository.location.LocationHelper;
import eim.yar.immo_task.domain.Shop;
import eim.yar.immo_task.domain.repository.ShopRepository;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * {@link ShopRepository} for loading and retrieving shop data.
 */
@Singleton
public class ShopDataRepository implements ShopRepository {

    /**
     * Max number of objects in a page.
     */
    public static final int PAGE_SIZE = 30;

    /**
     * Object to load shop data.
     */
    private final ShopDataLoader dataLoader;

    /**
     * Object to store shop data.
     */
    private final ShopDataStore dataStore;

    /**
     * Object to get current location.
     */
    private final LocationHelper locationHelper;

    /**
     * Object to transform a {@link eim.yar.immo_task.data.entity.ShopEntity} objects
     * into an {@link Shop} objects.
     */
    private final ShopEntityDataMapper dataMapper;

    /**
     * Constructs a {@link ShopRepository}.
     *
     * @param loader A {@link ShopDataLoader} object to load shop data
     * @param store A {@link ShopDataStore} object to store shop data
     * @param location A {@link LocationHelper} object to get current location
     * @param mapper A {@link ShopEntityDataMapper} object to transform
     * {@link eim.yar.immo_task.data.entity.ShopEntity} objects to {@link Shop} objects
     */
    @Inject
    public ShopDataRepository(ShopDataLoader loader, ShopDataStore store,
                              LocationHelper location,
                              ShopEntityDataMapper mapper) {
        dataLoader = loader;
        dataStore = store;
        locationHelper = location;
        dataMapper = mapper;
    }

    @Override
    public Observable<Integer> loadShops() {
        return Observable.zip(dataLoader.loadShopEntities().observeOn(Schedulers.io()),
                locationHelper.currentLocation().observeOn(Schedulers.computation()),
                (shopEntities, currentLocation) -> {
                    for (ShopEntity shopEntity : shopEntities) {
                        Location shopPoint = new Location("shop");
                        shopPoint.setLatitude(shopEntity.getLatitude());
                        shopPoint.setLongitude(shopEntity.getLongitude());
                        shopEntity.setDistance(currentLocation.distanceTo(shopPoint));
                    }
                    dataStore.saveShops(shopEntities);
                    return shopEntities.size();
                });
    }

    @Override
    public Observable<List<Shop>> getShopListPage(int pageNumber) {
        return dataStore.shopEntityList(PAGE_SIZE, (pageNumber - 1) * PAGE_SIZE)
                .map(dataMapper::transform);
    }
}
