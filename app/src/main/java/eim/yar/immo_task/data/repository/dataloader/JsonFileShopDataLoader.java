package eim.yar.immo_task.data.repository.dataloader;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import eim.yar.immo_task.data.entity.ShopEntity;
import io.reactivex.Observable;

/**
 * {@link ShopDataLoader} implementation based on json file data store.
 *
 * Loads list of {@link ShopEntity} from json file in the following format:
 * array of shop objects is declared under "shop" member name
 * {"shop":[{}, {}, ...]}
 */
@Singleton
public class JsonFileShopDataLoader implements ShopDataLoader {

    /**
     * Type of shop data array loading from json file using Gson.
     */
    private static final Type SHOP_DATA_TYPE = new TypeToken<List<ShopEntity>>() {}.getType();

    /**
     * {@link Reader} objects provider to read json file.
     */
    private final Provider<Reader> readerProvider;

    /**
     * Object for using Gson.
     */
    private final Gson gson;

    /**
     * Construct a {@link ShopDataLoader} based on json file.
     *
     * @param readerProvider A {@link Reader} objects provider to read json file
     * with shops data.
     */
    @Inject
    public JsonFileShopDataLoader(Provider<Reader> readerProvider) {
        gson = new Gson();
        this.readerProvider = readerProvider;
    }

    @Override
    public Observable<List<ShopEntity>> loadShopEntities() {
        return Observable.create(emitter -> {
            Reader reader = readerProvider.get();
            JsonReader jsonReader = new JsonReader(reader);
            try {
                skipReaderToArray(reader);
                List<ShopEntity> shopEntities = gson.fromJson(jsonReader, SHOP_DATA_TYPE);
                emitter.onNext(shopEntities);
                emitter.onComplete();
            } catch (JsonParseException exception) {
                emitter.onError(exception);
            } finally {
                jsonReader.close();
            }
        });
    }

    /**
     * Skip json file reader to array of shop data objects.
     * @param reader json file reader
     * @throws IOException If an I/O error occurs
     */
    private void skipReaderToArray(Reader reader) throws IOException {
        int ch = reader.read();
        while (ch != ':' && ch != -1) {
            ch = reader.read();
        }
    }
}
