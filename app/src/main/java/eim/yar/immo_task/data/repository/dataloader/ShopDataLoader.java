package eim.yar.immo_task.data.repository.dataloader;

import java.util.List;

import eim.yar.immo_task.data.entity.ShopEntity;
import io.reactivex.Observable;

/**
 * Interface that represents a data loader from where {@link ShopEntity} objects are loaded.
 */
public interface ShopDataLoader {

    /**
     * Load List of {@link ShopEntity}.
     *
     * @return An {@link Observable} which will emit a List of loaded {@link ShopEntity}
     */
    Observable<List<ShopEntity>> loadShopEntities();
}
