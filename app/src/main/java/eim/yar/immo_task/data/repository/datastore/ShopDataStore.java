package eim.yar.immo_task.data.repository.datastore;

import java.util.List;

import eim.yar.immo_task.data.entity.ShopEntity;
import io.reactivex.Observable;

/**
 * Interface that represents a data store from where {@link ShopEntity} data objects are retrieved.
 */
public interface ShopDataStore {

    /**
     * Save list of {@link ShopEntity} data objects in a data store.
     *
     * @param shops List of {@link ShopEntity} data objects to store.
     */
    void saveShops(List<ShopEntity> shops);

    /**
     * Get list of {@link ShopEntity} from the data store.
     * @param limit max number of objects to get
     * @param offset number of skipped objects
     * @return an {@link Observable} which will emit a List of {@link ShopEntity}
     */
    Observable<List<ShopEntity>> shopEntityList(int limit, int offset);
}
