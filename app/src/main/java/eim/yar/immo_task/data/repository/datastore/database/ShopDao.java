package eim.yar.immo_task.data.repository.datastore.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import eim.yar.immo_task.data.entity.ShopEntity;

@Dao
public interface ShopDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertShops(List<ShopEntity> shops);

    @Query("SELECT * FROM shops ORDER BY distance, id LIMIT :limit OFFSET :offset")
    ShopEntity[] loadShops(int limit, int offset);
}
