package eim.yar.immo_task.data.repository.datastore.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import eim.yar.immo_task.data.entity.ShopEntity;

@Database(entities = {ShopEntity.class}, version = 2)
public abstract class ShopDatabase extends RoomDatabase {

    public static final String DB_NAME = "Shops.db";

    public abstract ShopDao shopDao();
}
