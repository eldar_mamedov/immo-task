package eim.yar.immo_task.data.repository.datastore.database;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import eim.yar.immo_task.data.entity.ShopEntity;
import eim.yar.immo_task.data.repository.datastore.ShopDataStore;
import io.reactivex.Observable;

/**
 * {@link ShopDataStore} implementation based on SQLite database data store.
 */
@Singleton
public class ShopDatabaseDataStore implements ShopDataStore {

    /**
     * Data access object for working with database.
     */
    private final ShopDao shopDao;

    /**
     * Construct a {@link ShopDataStore} based on SQLite database.
     *
     * @param dao {@link ShopDao} object for working with database.
     */
    @Inject
    public ShopDatabaseDataStore(ShopDao dao) {
        shopDao = dao;
    }

    @Override
    public void saveShops(List<ShopEntity> shops) {
        shopDao.insertShops(shops);
    }

    @Override
    public Observable<List<ShopEntity>> shopEntityList(int limit, int offset) {
        return Observable.create(emitter -> {
            emitter.onNext(Arrays.asList(shopDao.loadShops(limit, offset)));
            emitter.onComplete();
        });
    }
}
