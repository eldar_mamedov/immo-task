package eim.yar.immo_task.data.repository.location;

import android.location.Location;

import io.reactivex.Observable;

/**
 * Interface that represents a location provider to get current location.
 */
public interface LocationHelper {

    /**
     * Get current location.
     * @return An {@link Observable} which will emit a current location
     */
    Observable<Location> currentLocation();
}
