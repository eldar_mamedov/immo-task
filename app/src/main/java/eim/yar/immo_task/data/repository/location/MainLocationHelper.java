package eim.yar.immo_task.data.repository.location;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

/**
 * {@link LocationHelper} implementation.
 */
@Singleton
public class MainLocationHelper implements LocationHelper {

    /**
     * Default latitude value.
     */
    private static final double LATITUDE_MOSCOW = 55.75370903771494;

    /**
     * Default longitude value.
     */
    private static final double LONGITUDE_MOSCOW = 37.61981338262558;

    /**
     * Location request expiration duration in milliseconds.
     */
    private static final int LOCATION_REQUEST_DURATION = 10000;

    /**
     * Location request interval for active location updates in milliseconds.
     */
    private static final int LOCATION_REQUEST_INTERVAL = 60000;

    private final Context context;

    final Handler handler = new Handler();

    final Location defaultLocation = new Location("Moscow");

    Location currentLocation;

    ObservableEmitter<Location> emitter;

    /**
     * Location provider client to get device current location.
     */
    FusedLocationProviderClient locationProviderClient;

    /**
     * Location provider request contains quality of service parameters for location requests.
     */
    LocationRequest locationRequest;

    /**
     * Location request callback to update current location field and emit its value.
     */
    LocationCallback locationRequestCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult != null && locationResult.getLastLocation() != null) {
                currentLocation = locationResult.getLastLocation();
            }
            handler.post(emitLocationRunnable);
        }
    };

    /**
     * Runnable object to emit current location and to complete locating process.
     */
    Runnable emitLocationRunnable = new Runnable() {
        @Override
        public void run() {
            if (emitter != null) {
                emitter.onNext(currentLocation);
                emitter.onComplete();
            }
            emitter = null;
            handler.removeCallbacksAndMessages(null);
            locationProviderClient.removeLocationUpdates(locationRequestCallback);
            currentLocation = defaultLocation;
        }
    };

    /**
     * Construct a {@link MainLocationHelper}.
     * @param context application context
     */
    @Inject
    public MainLocationHelper(Context context) {
        this.context = context;
        initDefaultLocation();
        initLocationProvider();
        currentLocation = defaultLocation;
    }

    /**
     * Set latitude and longitude for default location.
     */
    private void initDefaultLocation() {
        defaultLocation.setLatitude(LATITUDE_MOSCOW);
        defaultLocation.setLongitude(LONGITUDE_MOSCOW);
    }

    /**
     * Initialize location provider client and location request.
     */
    private void initLocationProvider() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(LOCATION_REQUEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationProviderClient = LocationServices.getFusedLocationProviderClient(context);
    }

    @Override
    public Observable<Location> currentLocation() {
        return Observable.create(localeEmitter -> {
            emitter = localeEmitter;
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission
                    .ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                handler.postDelayed(emitLocationRunnable, LOCATION_REQUEST_DURATION);
                locationRequest.setExpirationDuration(LOCATION_REQUEST_DURATION);
                locationProviderClient.requestLocationUpdates(locationRequest,
                        locationRequestCallback, Looper.getMainLooper());
            } else {
                handler.post(emitLocationRunnable);
            }
        });
    }
}
