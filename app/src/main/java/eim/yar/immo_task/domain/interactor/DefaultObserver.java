package eim.yar.immo_task.domain.interactor;

import io.reactivex.observers.DisposableObserver;

/**
 * Default {@link DisposableObserver} base class to be used whenever you want default error handling.
 */
public class DefaultObserver<T> extends DisposableObserver<T> {

    @Override public void onNext(T t) {
        // empty by default
    }

    @Override public void onComplete() {
        // empty by default
    }

    @Override public void onError(Throwable exception) {
        // empty by default
    }
}
