package eim.yar.immo_task.domain.interactor;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import eim.yar.immo_task.domain.Shop;
import eim.yar.immo_task.domain.repository.ShopRepository;
import eim.yar.immo_task.presentation.injection.module.ApplicationModule;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * This class is an implementation of {@link Interactor} that represents a use case to
 * get a collection of {@link Shop} data objects from the repository.
 */
public class GetShopListPageInteractor extends
        Interactor<List<Shop>, GetShopListPageInteractor.Params> {

    private final ShopRepository shopRepository;

    /**
     * Constructs a {@link GetShopListPageInteractor}.
     *
     * @param shopRepository A {@link ShopRepository} for loading and retrieving shop data
     * @param jobScheduler A {@link Scheduler} to subscribe on
     * @param uiScheduler A {@link Scheduler} to observe on
     */
    @Inject
    public GetShopListPageInteractor(ShopRepository shopRepository,
                                 @Named(ApplicationModule.JOB) Scheduler jobScheduler,
                                 @Named(ApplicationModule.UI) Scheduler uiScheduler) {
        super(jobScheduler, uiScheduler);
        this.shopRepository = shopRepository;
    }

    @Override
    Observable<List<Shop>> buildInteractorObservable(Params params) {
        return this.shopRepository.getShopListPage(params.pageNumber);
    }

    public static final class Params {

        final int pageNumber;

        private Params(int pageNumber) {
            this.pageNumber = pageNumber < 1 ? 1 : pageNumber;
        }

        public static Params forPage(int pageNumber) {
            return new Params(pageNumber);
        }
    }
}
