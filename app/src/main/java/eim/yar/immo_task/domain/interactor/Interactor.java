package eim.yar.immo_task.domain.interactor;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Abstract class for a Interactor. It represents a execution unit for different use cases.
 */
public abstract class Interactor<ResultType, ParameterType> {

    private final CompositeDisposable disposables;

    private final Scheduler jobScheduler;

    private final Scheduler uiScheduler;

    /**
     * Constructs a {@link Interactor}.
     *
     * @param jobScheduler A {@link Scheduler} to subscribe on
     * @param uiScheduler A {@link Scheduler} to observe on
     */
    public Interactor(Scheduler jobScheduler, Scheduler uiScheduler) {
        this.jobScheduler = jobScheduler;
        this.uiScheduler = uiScheduler;
        this.disposables = new CompositeDisposable();
    }

    /**
     * Builds an {@link Observable} which will be used when executing the current {@link Interactor}.
     */
    abstract Observable<ResultType> buildInteractorObservable(ParameterType parameter);

    /**
     * Executes the current interactor.
     *
     * @param observer {@link DisposableObserver} which will be listening to the observable build
     * by {@link #buildInteractorObservable(ParameterType)} ()} method.
     * @param parameter Parameters (Optional) used to build/execute this interactor.
     */
    public void execute(ParameterType parameter, DisposableObserver<ResultType> observer) {
        final Observable<ResultType> observable = buildInteractorObservable(parameter)
                .subscribeOn(jobScheduler)
                .observeOn(uiScheduler);
        addDisposable(observable.subscribeWith(observer));
    }

    /**
     * Executes the current interactor.
     *
     * @param observer {@link DisposableObserver} which will be listening to the observable build
     * by {@link #buildInteractorObservable(ParameterType)} ()} method.
     */
    public void execute(DisposableObserver<ResultType> observer) {
        execute(null, observer);
    }

    /**
     * Dispose from current {@link CompositeDisposable}.
     */
    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    /**
     * Dispose from current {@link CompositeDisposable}.
     */
    private void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }
}
