package eim.yar.immo_task.domain.interactor;

import javax.inject.Inject;
import javax.inject.Named;

import eim.yar.immo_task.domain.Shop;
import eim.yar.immo_task.domain.repository.ShopRepository;
import eim.yar.immo_task.presentation.injection.module.ApplicationModule;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * This class is an implementation of {@link Interactor} that represents a use case for
 * loading and storing a collection of {@link Shop} data objects and return number of
 * loaded objects.
 */
public class LoadShopListInteractor extends Interactor<Integer, Void> {

    private final ShopRepository shopRepository;

    /**
     * Constructs a {@link LoadShopListInteractor}.
     *
     * @param shopRepository A {@link ShopRepository} for loading and retrieving shop data
     * @param jobScheduler A {@link Scheduler} to subscribe on
     * @param uiScheduler A {@link Scheduler} to observe on
     */
    @Inject
    public LoadShopListInteractor(ShopRepository shopRepository,
                                  @Named(ApplicationModule.JOB) Scheduler jobScheduler,
                                  @Named(ApplicationModule.UI) Scheduler uiScheduler) {
        super(jobScheduler, uiScheduler);
        this.shopRepository = shopRepository;
    }

    @Override
    Observable<Integer> buildInteractorObservable(Void unused) {
        return this.shopRepository.loadShops();
    }
}
