package eim.yar.immo_task.domain.repository;

import java.util.List;

import eim.yar.immo_task.domain.Shop;
import io.reactivex.Observable;

/**
 * Interface that represents a Repository for getting Shop related data.
 */
public interface ShopRepository {

    /**
     * Load shop data from {@link eim.yar.immo_task.data.repository.dataloader.ShopDataLoader}
     * object and store in {@link eim.yar.immo_task.data.repository.datastore.ShopDataStore} object.
     * @return An {@link Observable} which will emit a number of loaded {@link Shop} data objects
     */
    Observable<Integer> loadShops();

    /**
     * Get list of {@link Shop} for specified page from the repository.
     * @param pageNumber page number for which need to get objects to
     * @return an {@link Observable} which will emit a List of {@link Shop}
     */
    Observable<List<Shop>> getShopListPage(int pageNumber);
}
