package eim.yar.immo_task.presentation.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import eim.yar.immo_task.presentation.injection.module.ApplicationModule;
import eim.yar.immo_task.presentation.injection.module.DataModule;
import eim.yar.immo_task.presentation.view.activity.MainActivity;

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton
@Component(modules = {ApplicationModule.class, DataModule.class})
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
}
