package eim.yar.immo_task.presentation.injection.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eim.yar.immo_task.data.repository.ShopDataRepository;
import eim.yar.immo_task.data.repository.dataloader.JsonFileShopDataLoader;
import eim.yar.immo_task.data.repository.dataloader.ShopDataLoader;
import eim.yar.immo_task.data.repository.datastore.ShopDataStore;
import eim.yar.immo_task.data.repository.datastore.database.ShopDao;
import eim.yar.immo_task.data.repository.datastore.database.ShopDatabase;
import eim.yar.immo_task.data.repository.datastore.database.ShopDatabaseDataStore;
import eim.yar.immo_task.data.repository.location.LocationHelper;
import eim.yar.immo_task.data.repository.location.MainLocationHelper;
import eim.yar.immo_task.domain.repository.ShopRepository;

/**
 * Dagger module that provides data related objects.
 */
@Module
public class DataModule {

    @Provides
    Reader provideShopJsonFileReader(Context context) {
        try {
            return new BufferedReader(
                    new InputStreamReader(context.getAssets().open("shops.json"),
                            Charset.forName("UTF-8")));
        } catch (IOException exception) {
            return null;
        }
    }

    @Provides @Singleton
    ShopDatabase provideShopDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                ShopDatabase.class, ShopDatabase.DB_NAME)
                .fallbackToDestructiveMigration().build();
    }

    @Provides @Singleton
    ShopDao provideShopDao(ShopDatabase database) {
        return database.shopDao();
    }

    @Provides @Singleton
    ShopDataLoader provideShopDataLoader(JsonFileShopDataLoader loader) {
        return loader;
    }

    @Provides @Singleton
    LocationHelper provideLocationHelper(MainLocationHelper helper) {
        return helper;
    }

    @Provides @Singleton
    ShopDataStore provideShopDataStore(ShopDatabaseDataStore store) {
        return store;
    }

    @Provides @Singleton
    ShopRepository provideShopRepository(ShopDataRepository repository) {
        return repository;
    }
}
