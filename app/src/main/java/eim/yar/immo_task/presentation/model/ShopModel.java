package eim.yar.immo_task.presentation.model;

/**
 * Class that represents a shop data in the presentation layer.
 */
@SuppressWarnings("PMD.DataClass")
public class ShopModel {

    private final long id;

    private String name;

    private String address;

    private String metro;

    private String openingHours;

    private float distance;

    public ShopModel(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMetro() {
        return metro;
    }

    public void setMetro(String metro) {
        this.metro = metro;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }
}
