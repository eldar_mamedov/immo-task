package eim.yar.immo_task.presentation.model.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import eim.yar.immo_task.domain.Shop;
import eim.yar.immo_task.presentation.model.ShopModel;

/**
 * Mapper class used to transform {@link Shop} (in the domain layer) to {@link ShopModel} in the
 * presentation layer.
 */
@Singleton
public class ShopModelDataMapper {

    /**
     * Constructs a {@link ShopModelDataMapper}.
     */
    @Inject
    public ShopModelDataMapper() {
        // empty
    }

    /**
     * Transform a {@link Shop} into an {@link ShopModel}.
     *
     * @param shop Object to be transformed.
     * @return {@link ShopModel} if valid {@link Shop} otherwise null.
     */
    public ShopModel transform(Shop shop) {
        ShopModel shopModel = null;
        if (shop != null) {
            shopModel = new ShopModel(shop.getId());
            shopModel.setName(shop.getName());
            shopModel.setAddress(shop.getAddress());
            shopModel.setMetro(shop.getMetro());
            shopModel.setOpeningHours(shop.getOpeningHours());
            shopModel.setDistance(shop.getDistance());
        }
        return shopModel;
    }

    /**
     * Transform a List of {@link Shop} into a Collection of {@link ShopModel}.
     *
     * @param shopCollection Object Collection to be transformed.
     * @return {@link ShopModel} collection if valid {@link Shop} otherwise null.
     */
    public List<ShopModel> transform(Collection<Shop> shopCollection) {
        final List<ShopModel> shopModelList = new ArrayList<>();
        for (Shop shop : shopCollection) {
            final ShopModel shopModel = transform(shop);
            if (shopModel != null) {
                shopModelList.add(shopModel);
            }
        }
        return shopModelList;
    }
}
