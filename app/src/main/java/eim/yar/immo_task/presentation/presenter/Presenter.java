package eim.yar.immo_task.presentation.presenter;

/**
 * Interface representing a Presenter in a MVP pattern.
 */
public interface Presenter {

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * onResume() method.
     */
    void resume();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * onPause() method.
     */
    void pause();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * onDestroy() method.
     */
    void destroy();
}
