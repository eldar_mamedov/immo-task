package eim.yar.immo_task.presentation.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import eim.yar.immo_task.domain.Shop;
import eim.yar.immo_task.domain.interactor.DefaultObserver;
import eim.yar.immo_task.domain.interactor.GetShopListPageInteractor;
import eim.yar.immo_task.domain.interactor.LoadShopListInteractor;
import eim.yar.immo_task.presentation.model.mapper.ShopModelDataMapper;
import eim.yar.immo_task.presentation.view.ShopDataView;

/**
 * {@link Presenter} for shop data views.
 */
public class ShopPresenter implements Presenter {

    ShopDataView shopDataView;

    private final LoadShopListInteractor loadShopListInteractor;

    private final GetShopListPageInteractor getShopListPageInteractor;

    final ShopModelDataMapper shopModelDataMapper;

    int currentPage = 0;

    /**
     * Constructs a {@link ShopPresenter}.
     * @param loadInteractor {@link eim.yar.immo_task.domain.interactor.Interactor} that
     * represents a use case for loading and storing a collection of shop data
     * @param getInteractor {@link eim.yar.immo_task.domain.interactor.Interactor} that
     * represents a use case to get a collection of shop data to show
     * @param mapper {@link ShopModelDataMapper} object to transform
     * {@link Shop} objects to {@link eim.yar.immo_task.presentation.model.ShopModel} objects
     */
    @Inject
    public ShopPresenter(LoadShopListInteractor loadInteractor,
                         GetShopListPageInteractor getInteractor,
                         ShopModelDataMapper mapper) {
        loadShopListInteractor = loadInteractor;
        getShopListPageInteractor = getInteractor;
        shopModelDataMapper = mapper;
    }

    /**
     * Set view to work with.
     * @param view A view that implements {@link ShopDataView} interface
     */
    public void setView(@NonNull ShopDataView view) {
        shopDataView = view;
    }

    @Override
    public void resume() {
        // empty
    }

    @Override
    public void pause() {
        // empty
    }

    @Override
    public void destroy() {
        loadShopListInteractor.dispose();
        shopDataView = null;
    }

    /**
     * Handle click of the load collection button. Execute use case to load shop data.
     */
    public void onLoadButtonClicked() {
        if (shopDataView != null) {
            shopDataView.showShopDataLoading();
            loadShopListInteractor.execute(new ShopLoadObserver());
        }
    }

    /**
     * Execute use case to get next page of shop data.
     */
    public void showNextShopPage() {
        if (shopDataView != null) {
            shopDataView.showShopPageLoading();
            getShopListPageInteractor.execute(GetShopListPageInteractor.Params
                    .forPage(currentPage + 1), new GetShopListPageObserver());
        }
    }

    /**
     * Observer for {@link LoadShopListInteractor} use case result.
     */
    class ShopLoadObserver extends DefaultObserver<Integer> {

        @Override
        public void onNext(Integer size) {
            if (shopDataView != null) {
                shopDataView.hideShopDataLoading();
                shopDataView.showNumberOfLoadedShops(size);
                currentPage = 0;
                showNextShopPage();
            }
        }

        @Override
        public void onError(Throwable exception) {
            if (shopDataView != null) {
                shopDataView.hideShopDataLoading();
                shopDataView.showMessage(exception.getMessage());
            }
        }
    }

    /**
     * Observer for {@link GetShopListPageInteractor} use case result.
     */
    class GetShopListPageObserver extends DefaultObserver<List<Shop>> {

        @Override
        public void onNext(List<Shop> shopList) {
            if (shopDataView != null) {
                shopDataView.hideShopPageLoading();
                shopDataView.setIsEndOfShopList(shopList.isEmpty());
                if (currentPage++ == 0) {
                    shopDataView.clearShopListView();
                    shopDataView.setIsEndOfShopList(false);
                }
                shopDataView.addShopList(shopModelDataMapper.transform(shopList));
            }
        }
    }
}
