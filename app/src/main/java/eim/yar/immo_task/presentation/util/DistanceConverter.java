package eim.yar.immo_task.presentation.util;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import eim.yar.immo_task.R;

/**
 * Class with methods for working with distance.
 */
@Singleton
public class DistanceConverter {

    private final Context context;

    @Inject
    public DistanceConverter(Context context) {
        this.context = context;
    }

    /**
     * Convert distance value to string.
     * @param distance distance value
     * @return distance string
     */
    public String convertDistanceToString(float distance) {
        if (distance < 1000) {
            return context.getString(R.string.distance_meters, (int) distance);
        } else {
            return context.getString(R.string.distance_kilometers, distance / 1000);
        }
    }
}
