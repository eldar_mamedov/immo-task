package eim.yar.immo_task.presentation.view;

import java.util.List;

import eim.yar.immo_task.presentation.model.ShopModel;

/**
 * Interface representing a View in MVP pattern.
 * It is used as a view representing a shop data.
 */
public interface ShopDataView {

    /**
     * Show a view with a message indicating a shop data loading process.
     */
    void showShopDataLoading();

    /**
     * Hide a shop data loading view.
     */
    void hideShopDataLoading();

    /**
     * Show a message.
     * @param message A message string
     */
    void showMessage(String message);

    /**
     * Show number of loaded shops.
     * @param size A number of loaded shops
     */
    void showNumberOfLoadedShops(int size);

    /**
     * Clear shop list view in the UI.
     */
    void clearShopListView();

    /**
     * Add a shop list in the UI.
     *
     * @param shopModelList The list of {@link ShopModel} that will be shown
     */
    void addShopList(List<ShopModel> shopModelList);

    /**
     * Show a progress bar in the end of recycler view.
     */
    void showShopPageLoading();

    /**
     * Hide a progress bar in the end of recycler view.
     */
    void hideShopPageLoading();

    /**
     * Set indicator what all shop object are shown.
     * @param endOfList indicate is all shop object are shown
     */
    void setIsEndOfShopList(boolean endOfList);
}
