package eim.yar.immo_task.presentation.view.activity;

import android.content.pm.PackageManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eim.yar.immo_task.IMMOTaskApplication;
import eim.yar.immo_task.R;
import eim.yar.immo_task.presentation.model.ShopModel;
import eim.yar.immo_task.presentation.presenter.ShopPresenter;
import eim.yar.immo_task.presentation.view.ShopDataView;
import eim.yar.immo_task.presentation.view.adapter.MainPagerAdapter;
import eim.yar.immo_task.presentation.view.adapter.MonthsAdapter;
import eim.yar.immo_task.presentation.view.adapter.ShopAdapter;
import eim.yar.immo_task.presentation.view.component.BlackoutAppBarLayout;
import eim.yar.immo_task.presentation.view.component.listener.AppBarStateChangeListener;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Main activity of the application.
 */
public class MainActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener, ShopDataView {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.appbar) BlackoutAppBarLayout appBar;

    @BindView(R.id.main_content_label) TextView mainContentLabel;

    @BindView(R.id.page_two_recycler_view) RecyclerView recyclerView;

    @BindView(R.id.load_shops_btn) Button loadShopsBtn;

    @Inject ShopPresenter shopPresenter;

    @Inject
    ShopAdapter shopAdapter;

    /**
     * Appbar offset changed listener to determine the appbar state.
     */
    private AppBarStateChangeListener appBarStateListener;

    /**
     * Indicate is all shop objects are shown.
     */
    boolean endOfShopList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((IMMOTaskApplication) getApplication()).getApplicationComponent().inject(this);
        shopPresenter.setView(this);
        initViews();
    }

    @Override public void onResume() {
        super.onResume();
        shopPresenter.resume();
    }

    @Override public void onPause() {
        super.onPause();
        shopPresenter.pause();
    }

    @Override
    protected void onDestroy() {
        appBar.removeOnOffsetChangedListener(appBarStateListener);
        shopPresenter.destroy();
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Initialize activity views.
     */
    private void initViews() {
        initMainContentHeight();
        initAppBarLayout();
        initViewPager();
        initPageOne();
        initPageTwo();
        swipeRefreshLayout.setOnRefreshListener(this);
     }

    /**
     * Initialize main content views height. Set its height to the half of the screen height.
     */
    private void initMainContentHeight() {
        int halfScreenHeight = getResources().getDisplayMetrics().heightPixels / 2;
        appBar.getLayoutParams().height = halfScreenHeight;
        mainContentLabel.getLayoutParams().height = halfScreenHeight;
    }

    /**
     * Initialize AppBarLayout. Set appbar state listener.
     */
    private void initAppBarLayout() {
        appBarStateListener = new MainAppBarStateChangeListener();
        appBar.addOnOffsetChangedListener(appBarStateListener);
    }

    /**
     * Initialize main view pager. Set adapter and setup tab layout with the view pager.
     */
    private void initViewPager() {
        ViewPager pager = findViewById(R.id.view_pager);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        pager.setAdapter(new MainPagerAdapter(this));
        tabLayout.setupWithViewPager(pager);
    }

    /**
     * Initialize views of the page one.
     */
    private void initPageOne() {
        TextView pageOneLabel = findViewById(R.id.page_one_label);
        StringBuilder labelText = new StringBuilder();
        for (int i = 0; i < 150; i++) {
            labelText.append("Tab content ");
        }
        pageOneLabel.setText(labelText.toString());
    }

    /**
     * Initialize views of the page two. Initialize recycler view.
     */
    private void initPageTwo() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation()));
        initShopAdapter();
        recyclerView.setAdapter(buildMonthsAdapter());
    }

    /**
     * Initialize adapter to show shop data objects in the recycler view.
     */
    private void initShopAdapter() {
        shopAdapter.setOnItemClickListener((position, shopModel) ->
                Toast.makeText(getBaseContext(),
                        getString(R.string.item_click, position), Toast.LENGTH_SHORT).show());
    }

    /**
     * Build adapter to show list of months in the recycler view.
     * @return adapter to show list of months
     */
    private MonthsAdapter buildMonthsAdapter() {
        MonthsAdapter monthsAdapter = new MonthsAdapter();
        monthsAdapter.setOnItemClickListener(position -> Toast.makeText(getBaseContext(),
                getString(R.string.item_click, position), Toast.LENGTH_SHORT).show());
        return monthsAdapter;
    }

    @OnClick(R.id.load_shops_btn) void onTestButtonClick() {
        Toast.makeText(this, R.string.loading_catalog, Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this,
                ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            shopPresenter.onLoadButtonClicked();
        } else {
            ActivityCompat.requestPermissions(this, new String[] {ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            shopPresenter.onLoadButtonClicked();
        }
    }

    @Override
    public void showShopDataLoading() {
        loadShopsBtn.setEnabled(false);
        mainContentLabel.setText(R.string.loading_catalog);
    }

    @Override
    public void hideShopDataLoading() {
        loadShopsBtn.setEnabled(true);
        mainContentLabel.setText(R.string.main_content);
    }

    @Override
    public void showMessage(String message) {
        mainContentLabel.setText(message);
    }

    @Override
    public void showNumberOfLoadedShops(int size) {
        showMessage(getString(R.string.objects_loaded, size));
    }

    @Override
    public void clearShopListView() {
        shopAdapter.clearShopList();
    }

    @Override
    public void addShopList(List<ShopModel> shopModelList) {
        if (recyclerView.getAdapter() instanceof MonthsAdapter) {
            switchRecyclerViewToShopList();
        }
        shopAdapter.addShopList(shopModelList);
    }

    @Override
    public void showShopPageLoading() {
        shopAdapter.setLoading(true);
    }

    @Override
    public void hideShopPageLoading() {
        shopAdapter.setLoading(false);
    }

    @Override
    public void setIsEndOfShopList(boolean endOfList) {
        endOfShopList = endOfList;
    }

    /**
     * Setup recycler view to show list of shops.
     */
    private void switchRecyclerViewToShopList() {
        recyclerView.setAdapter(shopAdapter);
        recyclerView.addOnScrollListener(new ShowNextPageOnScrollListener());
    }

    /**
     * Appbar state listener for the main activity. Manage swipe refresh layout availability
     * and reset pages scrolling according to appbar state.
     */
    class MainAppBarStateChangeListener extends AppBarStateChangeListener {

        @Override
        public void onStateChanged(AppBarLayout appBarLayout, State state) {
            if (state.equals(State.EXPANDED)) {
                swipeRefreshLayout.setEnabled(true);
                NestedScrollView scrollView = findViewById(R.id.page_one_scroll_view);
                scrollView.fling(0);
                scrollView.smoothScrollTo(0, 0);
                recyclerView.smoothScrollToPosition(0);
                recyclerView.postDelayed(() -> recyclerView.scrollToPosition(0), 200);
            } else {
                swipeRefreshLayout.setEnabled(false);
            }
        }
    }

    /**
     * Recycler view on scroll listener to determine when its time to show next page.
     */
    class ShowNextPageOnScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = recyclerView.getLayoutManager().getItemCount();
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView
                    .getLayoutManager()).findLastVisibleItemPosition();
            if (!shopAdapter.isLoading() && !endOfShopList && loadShopsBtn.isEnabled()
                    && totalItemCount <= (lastVisibleItemPosition + 1)) {
                shopPresenter.showNextShopPage();
            }
        }
    }
}
