package eim.yar.immo_task.presentation.view.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import eim.yar.immo_task.R;

/**
 * Pager adapter for the main screen viewpager.
 */
public class MainPagerAdapter extends PagerAdapter {

    /**
     * Number of pages.
     */
    public static final int PAGES_COUNT = 2;

    /**
     * Activity that contains viewpager and its page layouts.
     */
    private final Activity activity;

    /**
     * Constructor.
     * @param activity activity that contains viewpager and its page layouts
     */
    public MainPagerAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((View) view);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        switch (position) {
            case 0:
                return activity.findViewById(R.id.page_one_scroll_view);
            case 1:
                return activity.findViewById(R.id.page_two_recycler_view);
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = activity.getString(R.string.tab);
        switch (position) {
            case 0:
                return title + " 1";
            case 1:
                return title + " 2";
            default:
                return super.getPageTitle(position);
        }
    }

    @Override
    public int getCount() {
        return PAGES_COUNT;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
