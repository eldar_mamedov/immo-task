package eim.yar.immo_task.presentation.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apache.commons.text.WordUtils;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import eim.yar.immo_task.R;

/**
 * Recycler view adapter to show list of months for default locale.
 */
public class MonthsAdapter extends RecyclerView.Adapter<MonthsAdapter.MonthsViewHolder> {

    private final List<String> months = new ArrayList<>();

    private OnItemClickListener itemClickListener;

    public MonthsAdapter() {
        String[] localeMonths = new DateFormatSymbols(Locale.getDefault()).getMonths();
        for (int i = 0; i < 3; i++) {
            for (String month: localeMonths) {
                String capitalizedMonth = WordUtils.capitalize(month);
                months.add(capitalizedMonth + (i == 0 ? "" : Integer.toString(i)));
            }
        }
    }

    @Override
    public MonthsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_selectable_list_item, parent, false);
        return new MonthsViewHolder(convertView);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        itemClickListener = onItemClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull MonthsViewHolder holder, int position) {
        holder.monthLabel.setText(months.get(position));
        holder.itemView.setOnClickListener(view -> {
            if (null != itemClickListener) {
                itemClickListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return months.size();
    }

    /**
     * Interface for item click listeners.
     */
    public interface OnItemClickListener {
        void onItemClicked(int position);
    }

    /**
     * Recycler view item views holder to show list of months.
     */
    public class MonthsViewHolder extends RecyclerView.ViewHolder {

        @BindView(android.R.id.text1) TextView monthLabel;

        public MonthsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
