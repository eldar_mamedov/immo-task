package eim.yar.immo_task.presentation.view.adapter;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eim.yar.immo_task.R;
import eim.yar.immo_task.presentation.model.ShopModel;
import eim.yar.immo_task.presentation.util.DistanceConverter;

/**
 * Recycler view adapter to show list of shops.
 */
public class ShopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_PROGRESS_BAR_TYPE = 1;

    private List<ShopModel> shopList = new ArrayList<>();

    private OnItemClickListener itemClickListener;

    /**
     * Indicate is need to show progress bar view as last item in the list.
     */
    private boolean isLoading;

    private final Handler handler = new Handler();

    private final  DistanceConverter distanceConverter;

    @Inject
    public ShopAdapter(DistanceConverter converter) {
        distanceConverter = converter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_PROGRESS_BAR_TYPE) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);
            return new ProgressBarViewHolder(convertView);
        } else {
            View convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shop_list_item, parent, false);
            return new ShopViewHolder(convertView);
        }
    }

    public void setLoading(boolean loading) {
        if (isLoading != loading) {
            isLoading = loading;
            if (loading) {
                handler.post(() -> this.notifyItemRangeInserted(shopList.size(), 1));
            } else {
                this.notifyItemRemoved(shopList.size());
            }
        }
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        itemClickListener = onItemClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (position < shopList.size()) {
            ShopViewHolder shopHolder = (ShopViewHolder) holder;
            shopHolder.shopName.setText(shopList.get(position).getName());
            shopHolder.shopAddress.setText(shopList.get(position).getAddress());
            shopHolder.shopOpeningHours.setText(shopList.get(position).getOpeningHours());
            shopHolder.shopDistance.setText(distanceConverter
                    .convertDistanceToString(shopList.get(position).getDistance()));
            String metro = shopList.get(position).getMetro();
            if (metro == null) {
                shopHolder.shopMetroLayout.setVisibility(View.GONE);
            } else {
                shopHolder.shopMetroLayout.setVisibility(View.VISIBLE);
                shopHolder.shopMetro.setText(metro);
            }
            holder.itemView.setOnClickListener(view -> {
                if (null != itemClickListener) {
                    itemClickListener.onItemClicked(position, shopList.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return shopList.size() + (isLoading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= shopList.size()) {
            return VIEW_PROGRESS_BAR_TYPE;
        }
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        if (position < shopList.size()) {
            return shopList.get(position).getId();
        }
        return super.getItemId(position);
    }

    /**
     * Clear collection of shops to show.
     */
    public void clearShopList() {
        int size = this.shopList.size();
        this.shopList.clear();
        this.notifyItemRangeRemoved(0, size);
    }

    /**
     * Set collection of shops to show.
     * @param shopList collection of shops to show
     */
    public void setShopList(@NonNull List<ShopModel> shopList) {
        this.shopList = shopList;
        this.notifyDataSetChanged();
    }

    /**
     * Add collection of shops to show.
     * @param shopList collection of shops to show
     */
    public void addShopList(@NonNull List<ShopModel> shopList) {
        int startPosition = this.shopList.size();
        this.shopList.addAll(shopList);
        this.notifyItemRangeInserted(startPosition, shopList.size());
    }

    /**
     * Interface for item click listeners.
     */
    public interface OnItemClickListener {
        void onItemClicked(int position, ShopModel shopModel);
    }

    /**
     * Recycler view item views holder to show list of shops.
     */
    public class ShopViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name) TextView shopName;

        @BindView(R.id.address) TextView shopAddress;

        @BindView(R.id.metro_layout) LinearLayout shopMetroLayout;

        @BindView(R.id.metro) TextView shopMetro;

        @BindView(R.id.opening_hours) TextView shopOpeningHours;

        @BindView(R.id.distance) TextView shopDistance;

        public ShopViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Recycler view item views holder to show progress bar.
     */
    public class ProgressBarViewHolder extends RecyclerView.ViewHolder {

        public ProgressBarViewHolder(View itemView) {
            super(itemView);
        }
    }
}
