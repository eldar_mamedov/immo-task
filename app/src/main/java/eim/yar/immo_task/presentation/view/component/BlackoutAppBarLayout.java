package eim.yar.immo_task.presentation.view.component;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.util.AttributeSet;

/**
 * App bar layout that blackout on scroll.
 */
public class BlackoutAppBarLayout extends AppBarLayout {

    /**
     * Alpha value for the fully opaque drawable.
     */
    private static final int OPAQUE_ALPHA = 255;

    /**
     * Simple constructor to use when creating a view from code.
     * @param context The Context the view is running in
     */
    public BlackoutAppBarLayout(Context context) {
        this(context, null);
    }

    /**
     * Constructor that is called when inflating a view from XML.
     * @param context The Context the view is running in
     * @param attrs A collection of attributes, as found associated with a tag
     * in an XML document
     */
    public BlackoutAppBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initBlackoutEffect();
    }

    /**
     * Add offset changed listener to black out appbar when it is scrolled.
     */
    private void initBlackoutEffect() {
        addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float offsetRatio = ((float) Math.abs(verticalOffset))
                    / appBarLayout.getTotalScrollRange();
            int alpha = (int) (OPAQUE_ALPHA * offsetRatio);
            appBarLayout.getBackground().setAlpha(alpha);
        });
    }
}
