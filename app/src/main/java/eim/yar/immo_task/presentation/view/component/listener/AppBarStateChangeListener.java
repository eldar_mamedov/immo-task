package eim.yar.immo_task.presentation.view.component.listener;

import android.support.design.widget.AppBarLayout;

/**
 * Appbar offset changed listener to determine the appbar state.
 */
public abstract class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

    /**
     * Contains appbar state types.
     */
    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    /**
     * Current appbar state.
     */
    private State currentState = State.IDLE;

    @Override
    public final void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0) {
            if (currentState != State.EXPANDED) {
                onStateChanged(appBarLayout, State.EXPANDED);
            }
            currentState = State.EXPANDED;
        } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
            if (currentState != State.COLLAPSED) {
                onStateChanged(appBarLayout, State.COLLAPSED);
            }
            currentState = State.COLLAPSED;
        } else {
            if (currentState != State.IDLE) {
                onStateChanged(appBarLayout, State.IDLE);
            }
            currentState = State.IDLE;
        }
    }

    /**
     * Notifies on state change.
     * @param appBarLayout appbar layout
     * @param state collapse state
     */
    public abstract void onStateChanged(AppBarLayout appBarLayout, State state);
}
